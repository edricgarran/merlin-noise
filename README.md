Merlin Noise - Meta Perlin Noise
================================

Metaprogrammed n-dimensional perlin-noise implementation.
* Basic usage exemplified in _test.cpp_.
* Test script generating an animated demonstration of 3D noise in _test.sh_.
