#include <cmath>
#include <vector>


namespace math
{
    constexpr int floor(double x)
    {
        return (x > 0) ? x : x - 1;
    }

    constexpr double linear_interpolate(double a, double b, double x)
    {
        return (1.0 - x) * a + x * b;
    }

    template <typename T, typename U>
    constexpr auto log(T base, U x) {
        return log10(x) / log10(base);
    }
}


namespace util
{
    template <typename F>
    constexpr auto reverse_args(F callback) { return callback(); }
    template <typename F, typename T, typename ...Args>
    constexpr auto reverse_args(F callback, T a0, Args ...args)
    {
        return reverse_args([=](auto ...args) {
            return callback(args..., a0);
        }, args...);
    }


    template <typename F, typename T>
    constexpr auto reduce(F func, T acc) { return acc; }
    template <typename F, typename T, typename ...Xs>
    constexpr auto reduce(F func, T acc, T x, Xs ...xs)
    {
        return reduce(func, func(acc, x), xs...);
    }


    template <typename ...Args>
    constexpr auto max(Args ...args)
    {
        return reduce([](auto a, auto b) { return (a > b) ? a : b; }, args...);
    }


    template <typename ...Args>
    constexpr auto product(Args ...args)
    {
        return reduce([](auto a, auto b) { return a * b; }, 1, args...);
    }


    template <typename F>
    constexpr auto sequence(F f) { return f; }
    template <typename F, typename ...Fs>
    constexpr auto sequence(F f, Fs ...fs)
    {
        return [=](auto ...args) { return sequence(fs...)(f(args...)); };
    }
    template <typename ...Fs>
    constexpr auto do_(Fs ...fs)
    {
        return sequence(fs...)();
    }
}


namespace image {
    template <typename T, typename F, typename ...Args>
    auto impl_image(
        std::vector<T>& pixels, F fragment, int offset)
    {
        pixels[offset] = fragment();
    }
    template <typename T, typename F, typename ...Args>
    auto impl_image(
        std::vector<T>& pixels, F fragment, int offset,
        int size, Args... args)
    {
        auto stride = util::product(args...);
        for (int x{}; x < size; ++x) {
            impl_image(pixels,
                [=](auto ...args) { return fragment(args..., x); },
                offset + x * stride, args...
            );
        }
    }
    template <typename F, typename ...Args>
    auto image(F fragment, Args... args)
    {
        using T = decltype(fragment(args...));
        std::vector<T> pixels(util::product(args...));
        util::reverse_args([&](auto ...args) {
            impl_image(pixels, fragment, 0, args...);
        }, args...);
        return pixels;
    }
}


namespace merlin {
    /* Reduce dimensions of raw_noise parameters */
    constexpr int impl_dim_reduce(int acc, int) { return acc; }
    template <typename ...Xs>
    constexpr int impl_dim_reduce(int acc, int coef, int x, Xs ...xs)
    {
        return impl_dim_reduce(acc + x * coef, coef * 57, xs...);
    }
    template <typename ...Xs>
    constexpr int dim_reduce(Xs ...xs)
    {
        return impl_dim_reduce(0, 1, xs...);
    }

    /* Raw discrete noise. */
    template <typename ...Args>
    constexpr double raw_noise(Args ...args)
    {
        return util::do_(
            [=]() { return dim_reduce(args...); },
            [](int x) { return x << 13 ^ x; },
            [](int n) {
                return 1.0f -
                ((n * (n * n * 15731 + 789221) + 1376312589) & 0x7fffffff) /
                1073741824.0f;
            }
        );
    }


    /* Smooth noise function by using influence from neighbours. */
    template <typename F>
    constexpr double impl_smooth_noise(F raw, int dim, int offset)
    {
        return raw() / (1 << (dim + offset));
    }
    template <typename F, typename ...Args>
    constexpr double impl_smooth_noise(F raw, int dim, int offset,
        double x, Args ...args)
    {
        return
            impl_smooth_noise(
                [=](auto ...args) { return raw(args..., x + 1); },
                dim, offset + 1, args...) +
            impl_smooth_noise(
                [=](auto ...args) { return raw(args..., x - 1); },
                dim, offset + 1, args...) +
            impl_smooth_noise(
                [=](auto ...args) { return raw(args..., x    ); },
                dim, offset,     args...);
    }
    template <typename ...Args>
    constexpr double smooth_noise(Args ...args)
    {
        return util::reverse_args([](auto ...args) {
            return impl_smooth_noise(
                [](auto ...args) { return raw_noise(args...); },
                sizeof...(args), 0, args...
            );
        }, args...);
    }


    /* Variadic noise interpolation. */
    template <typename F>
    constexpr double impl_interpolate_noise(F smooth)
    {
        return smooth();
    }
    template <typename F, typename ...Args>
    constexpr double impl_interpolate_noise(F smooth, double x, Args ...args)
    {
        return math::linear_interpolate(
            impl_interpolate_noise(
                [=](auto ...args) {
                    return smooth(args..., math::floor(x));
                },
                args...
            ),
            impl_interpolate_noise(
                [=](auto ...args) {
                    return smooth(args..., math::floor(x) + 1);
                },
                args...
            ),
            x - math::floor(x)
        );
    }
    template <typename ...Args>
    constexpr double interpolate_noise(Args ...args)
    {
        return util::reverse_args([](auto ...args) {
            return impl_interpolate_noise(
                [](auto ...args) { return smooth_noise(args...); },
                args...
            );
        }, args...);
    }


    /* Main noise function, repeating algorithm with increasing
     * frequency and decreasing amplitude. */
    template <typename ...Args>
    constexpr double impl_noise(
        double total, double max_amp, double amplitude, double frequency,
        int oct, int oct_1, double persistence, Args ...args)
    {
        return (oct == oct_1)
            ? total / max_amp
            : impl_noise(
                total + amplitude * interpolate_noise(args * frequency...),
                max_amp + amplitude,
                amplitude * persistence,
                frequency * 2,
                oct + 1,
                oct_1,
                persistence,
                args...
            );
    }

    template <typename ...Args>
    constexpr
    double noise(int oct_0, int oct_1, double persistence, Args ...args)
    {
        return impl_noise(0, 0, 2, persistence,
            oct_0, oct_1, persistence, args...);
    }


    /* Generate perlin noise image */
    template <typename ...Args>
    auto image(double persistence, double zoom, Args ...args)
    {
        int size = util::max(args...);
        int depth = math::log(1 / persistence, size);
        int oct_0 = math::log(1 / persistence, size / zoom);
        int oct_1 = oct_0 + depth;
        return image::image([=](auto ...args) {
            return noise(oct_0, oct_1, persistence, args / zoom ...);
        }, args...);
    }
}
