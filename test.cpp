#include "merlin.h"
#include <fstream>
#include <sstream>


constexpr auto color(double value)
{
    return (value < 0)
        ? uint16_t((0.5 + value * 0.5) * 256)
        : uint16_t((value * 0.75 + 0.25) * 256) << 8;
}


int main(int argc, char *argv[])
{
    auto width = (argc > 1) ? atoi(argv[1]) : 256;
    auto height = (argc > 2) ? atoi(argv[2]) : 256;
    auto depth = (argc > 3) ? atoi(argv[3]) : 256;

    auto img = merlin::image(0.5, 32, width, height, depth);

    for (int z{}; z < depth; ++z) {
        std::stringstream filename;
        filename << 'o' << z << ".ppm";
        std::ofstream out_file(filename.str());
        out_file << "P3 " << width << ' ' << height << "\n255\n";
        for (int i{}; i < width * height; ++i) {
            auto c = color(img[z * width * height + i]);
            out_file
                << 0 << ' '
                << (c >> 8 & 0xff) << ' '
                << (c & 0xff) << ' ';
        }
    }


    return 0;
}
