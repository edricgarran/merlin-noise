#!/bin/bash

WIDTH=1366
HEIGHT=768
DEPTH=256

g++ -std=c++14 -Wall -pedantic -O2 test.cpp -o test
./test $WIDTH $HEIGHT $DEPTH
eval gm convert -delay 16 o{0..$(($DEPTH - 1))}.ppm -loop 0 anim.gif
rm *.ppm
